# GitFlow or GitLab-Flow: when to use what? and how to define RACI model.
## A unique perspective on git repository management

This article defines a unique perspective of combining two git-flow/branching models and understanding the pros & cons of each style of git management. 

Disclaimer: This perspective is from my experience. As any strategies, this is an opinionated view. Many variations of the below flow is possible which could be more suited for your project.

#### Gitflow
This flow or git branching model is the most commonly used flow. A detailed description of this flow is well described by the Atlassian article [here](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow)

The git branching models follow a path of feature requests and host patches bug fixes and finally merge the final changes to the master branch 
The Git flow is excellent for development and the branching models help in managing agile incremental lifecycle. The various feature/bug/hotfix branches help developers work independently on their tasks. The flow is well suited for releases at specific points using git tagging/release branches and reconciliation to the master branch. This works well when the output of the projects is specifically compiled binaries.

#### GitLab Flow
This git branching model is a newer model. A detailed description of this flow is here in the GitLab article [here](https://docs.gitlab.com/ee/topics/gitlab_flow.html) 
This git branching model follows environment-specific branching model dev/staging/prod etc, where there is a specific branch that always exists for specific environments. This git flow works well for projects which maintain multiple live environments. Projects which are using blue-green strategies are a great fit for this branching model. Projects which have deployments scripts managing environments vs those that focus on building binaries are a great fit for the GitLab flow.

### Comparing the git branching models

The individual flows - git or GitLab flow are great for a specific purpose.

| Context | Git Flow   |      GitLab Flow      | 
|:----------|:----------|:-------------|
| Source of truth | single source of truth in master branch |  specific branches for specific environment |
| Where to use |  works great for app dev projects, where code is still being developed |    works great for deployment projects   | 
| Code Ownership | Developer is the true owner even in deployment | Deployment teams / SREs | 
| Environments |  Simple Dev and Prod  | Multiple Environments Dev/QA/Cat/Pre-Prod/Prod Blue/Prod Green| 
| RACI | Developers are deployers and own the compiled artifacts | Multiple teams involved for specific functions(Dev / QA / Security ) own specific branches |
| Dependencies | Dependencies are managed at app dev level pom files in maven / package.json in node js, etc | When multiple projects are loosely coupled with its dependencies (like microservices) and multiple independent projects |
| Ease | easier as everything merges to main or master | Complex as too many branches to maintain  with multiple ownership |
| Learning curve | Simple single-purpose repo | Comparitively harder due to ownership of multiple branches and works mostly in matrix organizations |


### Combining the strategy

While the branching models have its pro /cons. It is possible to combine these models into a hybrid model.

Let's combine them, Here is a high-level view of it.
![image info](./img/flow.png)

In the proposed model,

The first half, the model follows Git-flow with the most commonly used branches like feature/bug branches and developers are expected on a regular cycle to merge into develop. Once the code is in develop branch it can go through the rigorous testing process. A completed tested state of the develop branch pushes to the release branch where it complies with an artifact(zip/jars etc) which is then ready to be deloyed to the dev environment.  

While we do have a dev environment the responsibility of develop branch vs the dev branch is slightly different. The develop branch which is an earlier stage in the flow is more defined for building on local laptops ( developer integration ), while the dev branch is more of the first shared state i.e. as an actual dev environment where integrated testing can be performed.

The dev environment branch is a shared state ( a common environment for multiple teams ) to help non-dev groups such as QA / Security / etc to start looking at preliminary code for progression. The dev environment is where full application tests can be performed. This dev environment branch can also co-exist with other sibling projects ( all deploying to the same environment )

From this point onwards all progression of the code is via the Gitlab flow, which had dedicated branches for each environment needed for the project.

Hot patches can also exist and will exist within the scope of the Gitlab-flow. However, it is necessary to force merge it back to develop branch.
Hot patches that don't need to be merged back to develop branch need to be a fork of an existing environment branch.

#### RACI model

Disclaimer: RACI Models wildly vary based on organization style ( matrix / hierarchical / etc ). This is a high-level generic model.

| Branch | Project  Owner | Developer | QA | Security | SRE | Deployer |
|:----------|:----------:|:-------------:|:----------:|:----------:|:-------------:|:-------------:|
| Feature |C|R|C|I|||
| Bug | C | R | C |I|||
| HotPatch | C | R | A | A | C| R |
| Develop |A|R|C|I|||
| Release |R|A|C|C|I|I|
| Dev |C|A|R|R|C|C|
| Test |A|C|R|R|A|A|
| Prod |R|I|A|A|R|R|

###### R - Responsible, A - Accountable, C - Consulted, I - Informed

Several variations of the above model are possible that can suit your organization. 

In my personal experience, I have seen organizations follow 2 repo models (separate repos for binary build and deployment), Combined models / just Git-flow / just Gitlab-flow. There is no one-size-fits-all, however, every organization needs to establish operating standards for day-to-day operations.
